TARGETS=vorlage.pdf

default: $(TARGETS)

#vorlage.pdf: vorlage.md
#	pandoc --template template -V lang=de $^ -o $@

vorlage.pdf: vorlage.tex
	pdflatex $^

.PHONY: clean
clean:
	$(RM) $(TARGETS)
