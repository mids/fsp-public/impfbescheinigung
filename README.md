# Vorlage für Impfbescheinigungen

Die in diesem Repository befindliche Vorlage wird vom FSP-Präsidium 2021 verwendet, um Impfbescheinigungen für Personen der dezentralen studentischen Selbstverwaltung nach Prioritätsstufe 3 auszustellen.

## How-To

1. [vorlage.tex](vorlage.tex) anpasse.
1. `make`
